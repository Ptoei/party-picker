import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PickerComponent } from "./picker/picker.component";
import { StatementsEditorComponent } from "./statements-editor/statements-editor.component";
import { ItemsEditorComponent } from "./items-editor/items-editor.component";
import { ExpertRatingComponent } from "./expert-rating/expert-rating.component";
import { LoginComponent } from "./login/login.component";
import { ExpertDashboardComponent } from "./expert-dashboard/expert-dashboard.component";
import { AdminDashboardComponent } from "./admin-dashboard/admin-dashboard.component";

const routes: Routes = [
  { path: "", component: PickerComponent },
  {
    path: ":project/picker",
    component: PickerComponent,
  },
  {
    path: "statements",
    component: StatementsEditorComponent,
  },
  {
    path: "items",
    component: ItemsEditorComponent,
  },
  {
    path: "expert-rating",
    component: ExpertRatingComponent,
  },
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "expert-dashboard",
    component: ExpertDashboardComponent,
  },
  {
    path: "admin-dashboard",
    component: AdminDashboardComponent,
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {}
