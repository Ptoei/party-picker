import { TestBed } from '@angular/core/testing';

import { UserScoringService } from './user-scoring.service';

describe('UserScoringService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserScoringService = TestBed.get(UserScoringService);
    expect(service).toBeTruthy();
  });
});
