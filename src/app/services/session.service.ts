import { Injectable } from "@angular/core";
import * as Realm from "realm-web";

import { take } from "rxjs/operators";
import { BehaviorSubject, combineLatest } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SessionService {
  // Mongo
  realmApp: Realm.App;

  mongo$ = new BehaviorSubject<any>(null);
  user$ = new BehaviorSubject<Realm.User>(null);
  project$ = new BehaviorSubject<any>(null);

  itemsCollection$ = new BehaviorSubject<any>(null);
  scoresCollection$ = new BehaviorSubject<any>(null);
  statementsCollection$ = new BehaviorSubject<any>(null);
  projectsCollection$ = new BehaviorSubject<any>(null);

  isExpert$ = new BehaviorSubject<boolean>(false);
  isAdmin$ = new BehaviorSubject<boolean>(false);
  isUser$ = new BehaviorSubject<boolean>(false);

  constructor() {
    // Mongo
    this.realmApp = new Realm.App({ id: "picker-qicvy" });

    this.user$.subscribe((user) => {
      if (user) {
        this.mongo$.next(user.mongoClient("mongodb-atlas"));
        this.isAdmin$.next(!!user.customData.admin);
        this.isExpert$.next(!!user.customData.expert);
        this.isUser$.next(!!user.customData.project);
      }
    });

    // For users with an account we emit project reference. For anonymous users "logging in"
    // through the picker page we will control this from the picker page. When there is already
    // a project set, we skip this procedure to avoid losing the project context upon logout.
    combineLatest([this.user$, this.projectsCollection$]).subscribe(
      ([user, projectsCollection]) => {
        this.project$.pipe(take(1)).subscribe((currentProject) => {
          if (projectsCollection && user && !currentProject) {
            projectsCollection
              .findOne({ active: true, _id: user.customData.project })
              .then((project) => {
                this.project$.next(project);
              });
          }
        });
      }
    );

    this.mongo$.subscribe((mongo) => {
      if (mongo) {
        this.itemsCollection$.next(mongo.db("main").collection("items"));
        this.statementsCollection$.next(
          mongo.db("main").collection("statements")
        );
        this.scoresCollection$.next(mongo.db("main").collection("scores"));
        this.projectsCollection$.next(mongo.db("main").collection("projects"));
      }
    });
  }

  async login(email?: string, password?: string) {
    var credentials: Realm.Credentials;
    if (email && password) {
      credentials = Realm.Credentials.emailPassword(email, password);
    } else if (!email && !password) {
      credentials = Realm.Credentials.anonymous();
    } else {
      console.log("Incomplete credentials");
      return;
    }
    // Create an anonymous credential
    try {
      // Authenticate the user
      const user = await this.realmApp.logIn(credentials);

      // `App.currentUser` updates to match the logged in user
      if (user.id === this.realmApp.currentUser.id) {
        this.user$.next(user);
      }
    } catch (err) {
      console.error("Failed to log in", err);
    }
  }

  logout() {
    return new Promise((resolve) => {
      this.user$.pipe(take(1)).subscribe((user) =>
        user.logOut().then(() => {
          this.user$.next(null);
          resolve("logged out");
        })
      );
    });
  }
}
