import { BehaviorSubject } from "rxjs";

import { IStatement } from "../interfaces/statement";

import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class UserScoringService {
  scoredStatements: IStatement[];
  _scoredStatements = new BehaviorSubject<IStatement[]>(null);

  constructor() {}

  updateScores(likedStatements: IStatement[], neutralStatements: IStatement[]) {
    this.scoredStatements = likedStatements.concat(neutralStatements);
    this._scoredStatements.next(this.scoredStatements);
  }
}
