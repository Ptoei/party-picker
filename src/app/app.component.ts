import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { SessionService } from "./services/session.service";
import { take } from "rxjs/operators";
import { VERSION } from "../environments/version";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "item-picker";
  loginMenuOpen: boolean = false;

  constructor(private router: Router, public sessionService: SessionService) {
    console.log("Git hash " + VERSION.raw);
  }

  toggleLoginMenu() {
    this.loginMenuOpen = !this.loginMenuOpen;
  }

  logout() {
    this.sessionService.project$.pipe(take(1)).subscribe((project) => {
      this.sessionService
        .logout()
        .then(() => this.router.navigate([project.url + "/picker"]));
    });
  }
}
