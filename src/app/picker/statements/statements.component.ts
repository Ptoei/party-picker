import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";
import { ActivatedRoute, RouteConfigLoadStart } from "@angular/router";

import { IStatement } from "../../interfaces/statement";
import { IPointerArea } from "../../interfaces/pointerarea"

import { UserScoringService } from "../../services/user-scoring.service";
import { SessionService } from "../../services/session.service";

import {
  Subscription,
  combineLatest,
  fromEvent,
  Observable,
  BehaviorSubject,
} from "rxjs";

import { take } from "rxjs/operators";
interface IIndexWithCount {
  index: number;
  count: number;
}

interface ILikedWithCount {
  liked: number;
  count: number;
}
@Component({
  selector: "app-statements",
  templateUrl: "./statements.component.html",
  styleUrls: ["./statements.component.css"],
})
export class StatementsComponent implements OnInit, OnDestroy {
  likedStatements: IStatement[];
  neutralStatements: IStatement[];

  // These are actually template variables but for some reason I need to declare them here
  // to make ng build --prod work.
  likeList: any;
  dislikeList: any;

  // Observable that coninuously emits pointer position while over the "scored" element plus behavior
  // subject that makes last position always available.
  pointerMoveEvent$: Observable<PointerEvent>;
  pointerOverArea$ = new BehaviorSubject<IPointerArea>(null);

  positionSubscription: Subscription;

  scoredRect: any;

  statementsSub: Subscription;

  constructor(
    private userScoringService: UserScoringService,
    private sessionService: SessionService
  ) { }

  ngOnInit() {
    // Convert pointermove event in "scored" element to observable.
    this.pointerMoveEvent$ = fromEvent<PointerEvent>(
      document.getElementById("scored"),
      "pointermove"
    );

    // Convert pointer position to booleans indicating the earea the user is over
    this.positionSubscription = this.pointerMoveEvent$.subscribe((pointerEvent) => {
      this.scoredRect = document
        .getElementById("scored")
        .getBoundingClientRect();

      // Left half of the scored element means like, right half means dislike.
      if (pointerEvent.x < this.scoredRect.left + this.scoredRect.width / 2
        && pointerEvent.x > this.scoredRect.left
        && pointerEvent.y >= this.scoredRect.top
        && pointerEvent.y <= this.scoredRect.bottom
      ) {
        this.pointerOverArea$.next({ liked: true, disliked: false, neutral: false });
      } else if (pointerEvent.x >= this.scoredRect.left + this.scoredRect.width / 2
        && pointerEvent.x <= this.scoredRect.right
        && pointerEvent.y >= this.scoredRect.top
        && pointerEvent.y <= this.scoredRect.bottom
      ) {
        this.pointerOverArea$.next({ liked: false, disliked: true, neutral: false });
      } else {
        this.pointerOverArea$.next({ liked: false, disliked: false, neutral: true });
      }
    });

    // Retrieve statements
    this.statementsSub = combineLatest([
      this.sessionService.user$,
      this.sessionService.statementsCollection$,
      this.sessionService.project$,
    ]).subscribe(([user, statementsCollection, project]) => {
      // We need to check if user is not null otherwise we get a race condition when redirecting
      // to picker after logout.
      if (statementsCollection && project && user) {
        statementsCollection
          .find({ active: true, project: project._id })
          .then((statements) => {
            this.resetScores(statements);
          });
      }
    });
  }

  resetScores(neutralStatements: IStatement[]) {
    this.neutralStatements = neutralStatements;
    this.likedStatements = [];

    this.calculateScores();
  }

  calculateScores() {
    const nLikedStatements = this.likedStatements.length;
    for (let i = 0; i < nLikedStatements; i++) {
      this.likedStatements[i].score = nLikedStatements - i;
    }

    for (const neutralStatement of this.neutralStatements) {
      neutralStatement.score = 0;
      neutralStatement.liked = 0;
    }
    this.userScoringService.updateScores(
      this.likedStatements,
      this.neutralStatements
    );
  }

  // Transfer the dropped item to the correct array and position.
  drop(dropEvent: CdkDragDrop<string[]>) {
    if (dropEvent.previousContainer === dropEvent.container) {
      moveItemInArray(
        dropEvent.container.data,
        dropEvent.previousIndex,
        dropEvent.currentIndex
      );
    } else {
      transferArrayItem(
        dropEvent.previousContainer.data,
        dropEvent.container.data,
        dropEvent.previousIndex,
        dropEvent.currentIndex
      );
    }

    // Update the score of the moved item.
    if (dropEvent.container.element.nativeElement.id == "scored") {
      // For items dropped on scored we need to set the liked field depending on the drop location.
      this.pointerOverArea$.pipe(take(1)).subscribe((pointerArea) => {
        console.log(pointerArea);
        if (pointerArea.liked == true) {
          this.likedStatements[dropEvent.currentIndex].liked = 1;
        } else {
          this.likedStatements[dropEvent.currentIndex].liked = -1;
        }
        this.calculateScores();
      });
    } else if (dropEvent.container.element.nativeElement.id == "neutral") {
      // For neutral scores we only need to trigger a score update.
      this.calculateScores();
    }
  }

  ngOnDestroy() {
    this.positionSubscription.unsubscribe();
  }
}
