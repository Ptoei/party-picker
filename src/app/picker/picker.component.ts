import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Subscription, combineLatest } from "rxjs";
import { take } from "rxjs/operators";

import { SessionService } from "../services/session.service";

import { StatementsComponent } from "./statements/statements.component";
import { ResultsComponent } from "./results/results.component";

@Component({
  templateUrl: "./picker.component.html",
  styleUrls: ["./picker.component.css"],
})
export class PickerComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private sessionService: SessionService
  ) {}
  credSub: Subscription;

  @ViewChild(StatementsComponent) statementsComponent: StatementsComponent;
  @ViewChild(ResultsComponent) resultsComponent: ResultsComponent;

  ngOnInit() {
    // Log in anoymously.
    this.sessionService.user$.pipe(take(1)).subscribe((user) => {
      if (!user) {
        this.sessionService.login();
      }
    });

    // Retrieve and emit the project ID after login.
    // Need to include user here to prevent race condition after expert/admin logout where
    // the user is already logged out but project$ still has data. That would make the call below fail.
    this.credSub = combineLatest([
      this.sessionService.projectsCollection$,
      this.route.params,
      this.sessionService.user$,
    ]).subscribe(([projectCollection, routeParams, user]) => {
      if (projectCollection && routeParams && user) {
        projectCollection
          .findOne({ active: true, url: routeParams["project"] })
          .then((project) => {
            this.sessionService.project$.next(project);
          });
      }
    });
  }

  ngOnDestroy() {
    this.credSub.unsubscribe();
  }
}
