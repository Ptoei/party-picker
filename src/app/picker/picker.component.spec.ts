import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { PickerComponent } from "./picker.component";

describe("RatingComponent", () => {
  let component: PickerComponent;
  let fixture: ComponentFixture<RatingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PickerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
