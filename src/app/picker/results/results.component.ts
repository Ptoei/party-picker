import { Component, OnDestroy, OnInit } from "@angular/core";

import { Subscription, BehaviorSubject, combineLatest } from "rxjs";

import { IItem } from "../../interfaces/item";
import { IStatement } from "../../interfaces/statement";
import { IScoreAggregate } from "../../interfaces/scoreAggregate";

import { UserScoringService } from "../../services/user-scoring.service";
import { SessionService } from "../../services/session.service";

@Component({
  selector: "app-results",
  templateUrl: "./results.component.html",
  styleUrls: ["./results.component.css"],
})
export class ResultsComponent implements OnDestroy, OnInit {
  items: IItem[];
  statements: IStatement[];
  expertWeights: IScoreAggregate[];

  errorMessage: string;

  scoredStatementsSubjectSubscription: Subscription;

  scoresSub: Subscription;
  aggregatedScoresSub: Subscription;
  itemsSub: Subscription;
  aggregatedScores$ = new BehaviorSubject<IScoreAggregate[]>(null);

  constructor(
    private userScoringService: UserScoringService,
    private sessionService: SessionService
  ) {}

  ngOnInit() {
    this.scoredStatementsSubjectSubscription = this.userScoringService._scoredStatements.subscribe(
      (scoredStatements) => {
        this.calculateRanking(scoredStatements);
      }
    );

    // Retrieve aggregated expert weights used for the ranking process.
    this.scoresSub = combineLatest([
      this.sessionService.user$,
      this.sessionService.scoresCollection$,
      this.sessionService.project$,
    ]).subscribe(([user, scoresCollection, project]) => {
      // We need to check if user is not null otherwise we get a race condition when redirecting
      // to picker after logout.
      if (scoresCollection && project && user) {
        scoresCollection
          .aggregate([
            { $match: { active: true, project: project._id } },
            {
              $group: {
                _id: { item: "$item", statement: "$statement" },
                item: { $first: "$item" },
                statement: { $first: "$statement" },
                weight: { $avg: "$score" },
              },
            },
          ])
          .then((aggregationResult) => {
            this.aggregatedScores$.next(aggregationResult);
          });
      }
    });

    this.aggregatedScoresSub = this.aggregatedScores$.subscribe((weights) => {
      this.expertWeights = weights;
    });

    this.itemsSub = combineLatest([
      this.sessionService.user$,
      this.sessionService.itemsCollection$,
      this.sessionService.project$,
    ]).subscribe(([user, itemsCollection, project]) => {
      // We need to check if user is not null otherwise we get a race condition when redirecting
      // to picker after logout.
      if (itemsCollection && project && user) {
        itemsCollection
          .find({ active: true, project: project._id })
          .then((items) => {
            this.items = items;
          });
      }
    });
  }

  calculateRanking(statements: IStatement[]) {
    if (statements && this.expertWeights && this.items) {
      for (let item of this.items) {
        let thisScore = 0;
        for (let statement of statements) {
          const thisExpertWeight = this.expertWeights.find(
            (obj) =>
              obj.statement.equals(statement._id) && obj.item.equals(item._id)
          );
          if (thisExpertWeight) {
            if (thisExpertWeight.weight) {
              thisScore =
                thisScore +
                statement.liked * statement.score * thisExpertWeight.weight;
            }
          }
        }
        item.score = thisScore;
      }
      this.items.sort((a, b) => (a.score > b.score ? -1 : 1));
    }
  }

  ngOnDestroy() {
    this.scoredStatementsSubjectSubscription.unsubscribe();
    this.aggregatedScoresSub.unsubscribe();
    this.scoresSub.unsubscribe();
    this.itemsSub.unsubscribe();
  }
}
