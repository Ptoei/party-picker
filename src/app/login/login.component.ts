import { Component, OnDestroy, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { SessionService } from "../services/session.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit, OnDestroy {
  error: string = null;

  constructor(private router: Router, private sessionService: SessionService) {}
  userSub: Subscription;

  ngOnInit() {
    this.userSub = this.sessionService.user$.subscribe((user) => {
      if (user) {
        if (user.customData.expert) {
          this.router.navigate(["/expert-dashboard"]);
        }
        if (user.customData.admin) {
          this.router.navigate(["/admin-dashboard"]);
        }
      }
    });
  }

  onSubmit(form: NgForm) {
    this.sessionService.login(form.value.email, form.value.password);
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }
}
