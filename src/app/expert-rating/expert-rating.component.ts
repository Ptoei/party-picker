import { Component, OnDestroy, OnInit } from "@angular/core";
import { UntypedFormGroup, UntypedFormArray, UntypedFormControl, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";

import { BehaviorSubject, combineLatest, Subscription } from "rxjs";
import { take } from "rxjs/operators";

import { SessionService } from "../services/session.service";

import { IItem } from "../interfaces/item";
import { IScore } from "../interfaces/score";
import { IStatement } from "../interfaces/statement";
import { InfoDialogComponent } from "../info-dialog/info-dialog.component";

@Component({
  selector: "app-experts",
  templateUrl: "./expert-rating.component.html",
  styleUrls: ["./expert-rating.component.css"],
})
export class ExpertRatingComponent implements OnInit, OnDestroy {
  items: IItem[] = [];
  scores: IScore[] = [];
  statements: IStatement[] = [];
  scoresForm: UntypedFormGroup;
  scoresFormArray: UntypedFormArray;

  instructions: string;
  itemSelected: boolean;

  items$ = new BehaviorSubject<IItem[]>(null);
  statements$ = new BehaviorSubject<IStatement[]>(null);
  selectedItemId$ = new BehaviorSubject<any>(null);
  itemStatements$ = new BehaviorSubject<any[]>(null); // Array of the statement objects of the currently selected item with gaps for scores that are not in the db yet.

  itemsCollectionSub: Subscription;
  statementsCollectionSub: Subscription;
  itemExpertScoresSub: Subscription;

  constructor(public dialog: MatDialog, public sessionService: SessionService) {
    this.scoresFormArray = new UntypedFormArray([]);

    this.scoresForm = new UntypedFormGroup({
      scores: this.scoresFormArray,
    });

    this.itemSelected = false;

    this.instructions = "fix this";
    // "<p>Select a " +
    // sessionService.project.itemDefinition.single +
    // " and fill in the scores in the table. The scores \
    // go from 0 (statement does not apply at all) to 100 \
    // (statement applies fully).</p> \
    // <p> If you score a " +
    // sessionService.project.itemDefinition.single +
    // ", then please add scores for all statements shown. Do not \
    // forget to hit save before moving on to another " +
    // sessionService.project.itemDefinition.single +
    // " or browsing away from this page.</p> \
    // <p>If you have unsufficient knowledge of a " +
    // sessionService.project.itemDefinition.single +
    // " then do not add any scores for that " +
    // sessionService.project.itemDefinition.single +
    // ".";
  }

  ngOnInit() {
    // NB: Copy of what's in items component, move to service?
    // Retrieve all items for current project and emit them.
    this.itemsCollectionSub = combineLatest([
      this.sessionService.itemsCollection$,
      this.sessionService.project$,
    ]).subscribe(([itemsCollection, project]) => {
      console.log(itemsCollection);
      console.log(project);
      if (itemsCollection && project) {
        itemsCollection
          .aggregate([
            {
              $match: {
                project: project._id,
              },
            },
            {
              $sort: {
                active: -1,
                name: 1,
              },
            },
          ])
          .then((items) => {
            this.items$.next(items);
          });
      }
    });

    // NB: Copy of what's in statements component, move to service?
    // Retrieve all statements for current project and emit them.
    this.statementsCollectionSub = combineLatest([
      this.sessionService.statementsCollection$,
      this.sessionService.project$,
    ]).subscribe(([statementsCollection, project]) => {
      console.log(statementsCollection);
      console.log(project);
      if (statementsCollection && project) {
        statementsCollection
          .aggregate([
            {
              $match: {
                project: project._id,
              },
            },
            {
              $sort: {
                active: -1,
                name: 1,
              },
            },
          ])
          .then((statements) => {
            this.statements$.next(statements);
          });
      }
    });

    // Changed the selected item triggers the assembling of the scores array.
    this.itemExpertScoresSub = this.selectedItemId$.subscribe((itemId) => {
      combineLatest([
        this.statements$.pipe(take(1)),
        this.sessionService.scoresCollection$.pipe(take(1)),
        this.sessionService.project$.pipe(take(1)),
        this.sessionService.user$.pipe(take(1)),
      ]).subscribe(([statements, scoresCollection, project, user]) => {
        if (statements && itemId) {
          scoresCollection
            .find({
              project: project._id,
              item: itemId,
              user: user.id,
            })
            .then((itemScores) => {
              this.scoresFormArray.controls = [];
              var thisItemScoreFull = []; // Array of all statement scores for current item, included ones without doc in db
              var thisItemScore;
              for (let thisStatement of statements) {
                // Check if the thisStatement exists in itemScores
                thisItemScore = itemScores.find((itemScore) => {
                  return (
                    itemScore.statement.toString() ===
                    thisStatement._id.toString()
                  );
                });
                this.scoresFormArray.push(
                  new UntypedFormGroup({
                    statement: new UntypedFormControl(
                      thisStatement.label,
                      Validators.required
                    ),
                    score: new UntypedFormControl(
                      thisItemScore ? thisItemScore.score : null,
                      [
                        Validators.required,
                        Validators.min(0),
                        Validators.max(100),
                      ]
                    ),
                  })
                );
                thisItemScoreFull.push(thisItemScore);
              }
              this.itemStatements$.next(thisItemScoreFull);
            });
        }
      });
    });
  }

  ngOnDestroy() {
    this.itemsCollectionSub.unsubscribe();
    this.statementsCollectionSub.unsubscribe();
    this.itemExpertScoresSub.unsubscribe();
  }

  onSelectItem(itemId) {
    this.selectedItemId$.next(itemId);
    this.itemSelected = true;
  }

  onSaveScores() {
    let index: number = 0;
    for (let dummy of this.scoresFormArray.controls) {
      if ((<UntypedFormArray>this.scoresForm.get("scores")).at(index).dirty) {
        this.saveScore(index);
      }
      index += 1;
    }
  }

  saveScore(index) {
    let scoreFormReference = (<UntypedFormArray>this.scoresForm.get("scores")).at(
      index
    );
    combineLatest([
      this.itemStatements$.pipe(take(1)),
      this.sessionService.scoresCollection$.pipe(take(1)),
      this.sessionService.project$.pipe(take(1)),
      this.sessionService.user$.pipe(take(1)),
      this.selectedItemId$.pipe(take(1)),
      this.statements$.pipe(take(1)),
    ]).subscribe(
      ([scores, scoresCollection, project, user, itemId, statements]) => {
        // For existing score objects we just update the score.
        if (scores[index]) {
          var updatedScore = scores[index];
          updatedScore.score = scoreFormReference.value.score;
          scoresCollection.updateOne({ _id: updatedScore._id }, updatedScore);
          console.log(updatedScore);
        } else {
          scoresCollection
            .insertOne({
              active: true,
              user: user.id,
              project: project._id,
              item: itemId,
              statement: statements[index]._id,
              score: scoreFormReference.value.score,
            })
            // Update the list of document indices in local scope so things don't go belly-up when the user
            // first adds a new score and then immediately modifies it.
            .then((id) => (scores[index] = id));
        }
      }
    );

    scoreFormReference.markAsPristine();
    scoreFormReference.markAsUntouched();
  }

  onCancelChanges() {
    let index: number = 0;
    for (let dummy of this.scoresFormArray.controls) {
      if ((<UntypedFormArray>this.scoresForm.get("scores")).at(index).dirty) {
        this.resetScore(index);
      }
      index += 1;
    }
  }

  resetScore(index) {
    this.itemStatements$.pipe(take(1)).subscribe((scores) => {
      let scoreFormReference = (<UntypedFormArray>this.scoresForm.get("scores")).at(
        index
      );

      scoreFormReference.setValue({
        statement: scoreFormReference.value.statement,
        score: scores[index].score,
      });

      scoreFormReference.markAsPristine();
      scoreFormReference.markAsUntouched();
    });
  }

  showInstructionsDialog(): void {
    this.dialog.open(InfoDialogComponent, {
      width: "400px",
      data: { html: this.instructions, title: "Instructions" },
    });
  }
}
