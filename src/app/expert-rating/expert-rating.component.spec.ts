import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { ExpertRatingComponent } from "./expert-rating.component";

describe("ExpertRatingComponent", () => {
  let component: ExpertRatingComponent;
  let fixture: ComponentFixture<ExpertRatingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ExpertRatingComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
