import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { itemsEditorComponent } from "./items-editor.component";

describe("itemsEditorComponent", () => {
  let component: itemsEditorComponent;
  let fixture: ComponentFixture<itemsEditorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [itemsEditorComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(itemsEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
