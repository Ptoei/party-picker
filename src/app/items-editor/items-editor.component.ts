import { Component, OnDestroy, OnInit } from "@angular/core";
import { UntypedFormGroup, UntypedFormArray, UntypedFormControl, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { BehaviorSubject, combineLatest, Subscription } from "rxjs";
import { take } from "rxjs/operators";

import { IItem } from "../interfaces/item";
import { InfoDialogComponent } from "../info-dialog/info-dialog.component";
import { SessionService } from "../services/session.service";
@Component({
  selector: "app-items-editor",
  templateUrl: "./items-editor.component.html",
  styleUrls: ["./items-editor.component.css"],
})
export class ItemsEditorComponent implements OnInit, OnDestroy {
  items$ = new BehaviorSubject<IItem[]>(null);

  itemsCollectionSub: Subscription;
  itemsSub: Subscription;

  itemsForm: UntypedFormGroup;
  newItemForm: UntypedFormGroup;
  itemsFormArray: UntypedFormArray;
  instructions: string;

  editingNewItem = false;

  constructor(
    public dialog: MatDialog,
    private sessionService: SessionService
  ) {
    this.itemsFormArray = new UntypedFormArray([]);

    this.itemsForm = new UntypedFormGroup({
      items: this.itemsFormArray,
    });

    this.newItemForm = new UntypedFormGroup({
      name: new UntypedFormControl("", Validators.required),
      url: new UntypedFormControl("", Validators.required),
      imageLink: new UntypedFormControl("", Validators.required),
    });

    this.instructions =
      "<p>On this page you can manage the items that are the topic of you decision maker. </p> \
    <p> You can mark items as inactive. Inactive items will \
    not be visible to users of your decision maker. They are, however, visible to your experts \
    and can be scored by them. Deactivated items can later be activated again. \
    You can deactivate an item if you no longer want \
    to include it in your decision maker. You can also \
    deactivate an item if it is newly added and you want to await sufficient \
    expert scores to be added before adding it to your decision maker.</p>";
  }

  ngOnInit() {
    // Retrieve all items for current project and emit them.
    this.itemsCollectionSub = combineLatest([
      this.sessionService.itemsCollection$,
      this.sessionService.project$,
    ]).subscribe(([itemsCollection, project]) => {
      if (itemsCollection && project) {
        itemsCollection
          .aggregate([
            {
              $match: {
                project: project._id,
              },
            },
            {
              $sort: {
                active: -1,
                name: 1,
              },
            },
          ])
          .then((items) => {
            this.items$.next(items);
          });
      }
    });

    // Update the UI to match any changes in items.
    this.itemsSub = this.items$.subscribe((items) => {
      this.itemsFormArray.controls = [];
      if (items) {
        for (let item of items) {
          this.itemsFormArray.push(
            new UntypedFormGroup({
              active: new UntypedFormControl(item.active, Validators.required),
              name: new UntypedFormControl(item.name, Validators.required),
              url: new UntypedFormControl(item.url, Validators.required),
              imageLink: new UntypedFormControl(item.imageLink, Validators.required),
            })
          );
        }
      }
    });
  }

  onSaveNew() {
    // Insert a new item in the database and then retrieve it to update the app.
    combineLatest([
      this.sessionService.itemsCollection$.pipe(take(1)),
      this.sessionService.project$.pipe(take(1)),
      this.items$.pipe(take(1)),
    ]).subscribe(([itemsCollection, project, items]) => {
      itemsCollection
        .insertOne({
          active: true,
          project: project._id,
          ...this.newItemForm.value,
        })
        .then((id) => {
          itemsCollection
            .findOne({ _id: id.insertedId })
            .then((newItem) => {
              items.push(newItem);
              this.items$.next(items);
            })
            .catch((error) => console.log(error));
        });
    });

    this.newItemForm.reset();
    this.editingNewItem = false;
  }

  onCancelNew() {
    this.newItemForm.reset();
    this.editingNewItem = false;
  }

  onSaveExisting(index) {
    let itemFormReference = (<UntypedFormArray>this.itemsForm.get("items")).at(index);
    combineLatest([
      this.sessionService.itemsCollection$.pipe(take(1)),
      this.items$.pipe(take(1)),
    ]).subscribe(([itemsCollection, items]) => {
      // Assemble the updated document; project and active need to be retrieved from db:
      //  - project because it's not on the form;
      //  - active because the toggle in the control is disabled during editing and that makes its value unavailable at this point in the code.
      const updatedItem = {
        project: items[index].project,
        active: items[index].active,
        ...itemFormReference.value,
      };

      // Update the document in the db, then retrieve it to keep ui consistent with db
      itemsCollection
        .updateOne({ _id: items[index]._id }, updatedItem)
        .then(() => {
          itemsCollection
            .findOne({ _id: items[index]._id })
            .then((updatedDoc) => {
              items[index] = updatedDoc;
              this.items$.next(items);
            })
            .catch((error) => console.log(error));
        });
    });

    // Reset the form state
    itemFormReference.markAsPristine();
    itemFormReference.markAsUntouched();
    itemFormReference.get("active").enable();
  }

  getItemsControls() {
    const items: any = this.itemsForm.get("items");
    return items.controls;
  }

  onCancelModify(index) {
    let itemFormReference = (<UntypedFormArray>this.itemsForm.get("items")).at(index);
    // Pluck the old item state from the items$ observable and revert form.
    this.items$.pipe(take(1)).subscribe((items) => {
      itemFormReference.setValue({
        active: items[index].active,
        name: items[index].name,
        url: items[index].url,
        imageLink: items[index].imageLink,
      });
    });

    // Update form state
    itemFormReference.markAsPristine();
    itemFormReference.get("active").enable();
  }

  onAdditemForm() {
    this.editingNewItem = true;
  }

  showInstructionsDialog(): void {
    this.dialog.open(InfoDialogComponent, {
      width: "400px",
      data: { html: this.instructions, title: "Instructions" },
    });
  }

  onToggleActive(index) {
    // Get the toggled item from db, update the active state and get updated doc from db to keep ui uptodate.
    combineLatest([
      this.sessionService.itemsCollection$.pipe(take(1)),
      this.items$.pipe(take(1)),
    ]).subscribe(([itemsCollection, items]) => {
      itemsCollection.findOne({ _id: items[index]._id }).then((item) => {
        // Here's the actual operation
        item.active = !item.active;
        itemsCollection.updateOne({ _id: items[index]._id }, item).then(() => {
          itemsCollection
            .findOne({ _id: items[index]._id })
            .then((item) => {
              items[index] = item;
              this.items$.next(items);
            })
            .catch((error) => console.log(error));
        });
      });

      // Reset form state
      (<UntypedFormArray>this.itemsForm.get("items")).at(index).markAsPristine();
    });
  }

  onKeydownNewStatement(event) {
    if (
      (event.key === "Enter" || (event.key === "s" && event.ctrlKey)) &&
      this.newItemForm.valid
    ) {
      this.onSaveNew();
    }
    if (event.key === "Escape" || (event.key === "z" && event.ctrlKey)) {
      this.onCancelNew();
    }
  }

  onKeydownEditStatement(event, index) {
    let itemsFormRef = (<UntypedFormArray>this.itemsForm.get("items")).at(index);
    if (
      (event.key === "Enter" || (event.key === "s" && event.ctrlKey)) &&
      itemsFormRef.valid
    ) {
      this.onSaveExisting(index);
    } else if (event.key === "Escape" || (event.key === "z" && event.ctrlKey)) {
      this.onCancelModify(index);
    } else {
      itemsFormRef.get("active").disable();
    }
  }

  ngOnDestroy() {
    this.itemsCollectionSub.unsubscribe();
    this.itemsSub.unsubscribe();
  }
}
