export interface IUser {
  displayName: string;
  userRef: any;
  projectRef: any;
  admin: boolean;
  expert: boolean;
}
