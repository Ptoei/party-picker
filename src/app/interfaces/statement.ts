import ObjectId from "mongodb";
export interface IStatement {
  _id: ObjectId;
  id: any; //deprecated after firebase is exit
  project: ObjectId;
  active: boolean;
  label: string;
  score: number;
  description: string;
  liked: number; //should be 1 or -1
}
