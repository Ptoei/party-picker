export interface IScore {
  id: string;
  project: any;
  active: boolean;
  expert: any;
  item: any;
  statement: any;
  score: number;
}
