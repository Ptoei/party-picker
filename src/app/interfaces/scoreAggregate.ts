// Interface for an aggregated score per statement*item combo.
import ObjectId from "mongodb";
export interface IScoreAggregate {
  _id: ObjectId;
  item: ObjectId;
  statement: ObjectId;
  weight: number; // The weight of this statement for this item is the median of the score values in the scores array.
}
