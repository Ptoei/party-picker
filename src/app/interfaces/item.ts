import ObjectId from "mongodb";
export interface IItem {
  _id: ObjectId;
  id: any; // deprecate with firebase
  project: ObjectId;
  active: boolean;
  name: string;
  url: string;
  imageLink: string;
  score?: number; // Field for keeping the score during the picking
}
