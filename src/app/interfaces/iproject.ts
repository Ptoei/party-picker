export interface IProject {
  id: string;
  url: string;
  title: string;
  description: string;
  itemDefinition: { single: string; plural: string };
}
