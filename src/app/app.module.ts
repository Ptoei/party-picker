import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { StatementsComponent } from "./picker/statements/statements.component";
import { ResultsComponent } from "./picker/results/results.component";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MatCardModule } from "@angular/material/card";
import { MatBadgeModule } from "@angular/material/badge";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatListModule } from "@angular/material/list";
import { MatSelectModule } from "@angular/material/select";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatDialogModule } from "@angular/material/dialog";

import { StatementsEditorComponent } from "./statements-editor/statements-editor.component";
import { PickerComponent } from "./picker/picker.component";
import { UserScoringService } from "./services/user-scoring.service";
import { environment } from "../environments/environment";
import { ItemsEditorComponent } from "./items-editor/items-editor.component";
import { ExpertRatingComponent } from "./expert-rating/expert-rating.component";
import { LoginComponent } from "./login/login.component";
import { ExpertDashboardComponent } from "./expert-dashboard/expert-dashboard.component";
import { AdminDashboardComponent } from "./admin-dashboard/admin-dashboard.component";
import { InfoDialogComponent } from "./info-dialog/info-dialog.component";

@NgModule({
    declarations: [
        AppComponent,
        StatementsComponent,
        ResultsComponent,
        StatementsEditorComponent,
        PickerComponent,
        ItemsEditorComponent,
        ExpertRatingComponent,
        LoginComponent,
        ExpertDashboardComponent,
        AdminDashboardComponent,
        InfoDialogComponent,
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatButtonToggleModule,
        DragDropModule,
        HttpClientModule,
        MatCardModule,
        MatBadgeModule,
        MatButtonModule,
        MatDialogModule,
        MatSlideToggleModule,
        MatToolbarModule,
        MatTooltipModule,
        MatFormFieldModule,
        MatListModule,
        MatSelectModule,
        MatSidenavModule,
        ReactiveFormsModule,
        FormsModule,
        MatInputModule,
        MatIconModule,
    ],
    providers: [UserScoringService],
    bootstrap: [AppComponent]
})
export class AppModule {}
