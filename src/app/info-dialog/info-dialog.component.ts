import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: "app-info-dialog",
  template: `<mat-card
    ><mat-card-title>{{ data.title }} </mat-card-title>
    <mat-card-content [innerHTML]="data.html"></mat-card-content
  ></mat-card>`,
  // styleUrls: ["./info-dialog.component.css"],
})
export class InfoDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit(): void {}
}
