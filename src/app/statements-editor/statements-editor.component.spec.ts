import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StatementsEditorComponent } from './statements-editor.component';

describe('StatementsEditorComponent', () => {
  let component: StatementsEditorComponent;
  let fixture: ComponentFixture<StatementsEditorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StatementsEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatementsEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
