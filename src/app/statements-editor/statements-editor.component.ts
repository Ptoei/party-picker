import { Component, OnDestroy, OnInit } from "@angular/core";
import { UntypedFormGroup, UntypedFormArray, UntypedFormControl, Validators } from "@angular/forms";

import { BehaviorSubject, combineLatest, Subscription } from "rxjs";
import { take } from "rxjs/operators";

import { MatDialog } from "@angular/material/dialog";

import { IStatement } from "../interfaces/statement";
import { InfoDialogComponent } from "../info-dialog/info-dialog.component";
import { SessionService } from "../services/session.service";
@Component({
  selector: "app-statements-editor",
  templateUrl: "./statements-editor.component.html",
  styleUrls: ["./statements-editor.component.css"],
})
export class StatementsEditorComponent implements OnInit, OnDestroy {
  statements$ = new BehaviorSubject<IStatement[]>(null);

  statementsCollectionSub: Subscription;
  statementsSub: Subscription;

  statementsForm: UntypedFormGroup;
  newStatementForm: UntypedFormGroup;
  statementsFormArray: UntypedFormArray;
  statements: IStatement[];

  editingNewStatement = false;

  instructions: string;

  constructor(
    public dialog: MatDialog,
    private sessionService: SessionService
  ) {
    this.statementsFormArray = new UntypedFormArray([]);

    this.statementsForm = new UntypedFormGroup({
      statements: this.statementsFormArray,
    });

    this.newStatementForm = new UntypedFormGroup({
      label: new UntypedFormControl("", Validators.required),
      description: new UntypedFormControl("", Validators.required),
    });

    this.instructions =
      "<p>On this page you can manage the statements by \
      which the items of interest are \
      judged. </p> <p> The experts will score the items on these statements \
      and the end users will sort these statements by how important they find them. \
      The decision support algorithm will then sort the items for the user \
      by multiplying the user rankings by the export scores.</p> \
      <p> You can mark statements as inactive. Inactive statements will \
      not be available to end users. They are, however, visible to your experts \
      and can be scored by them. Deactivated statements can later be activated again. \
      You can deactivate a statement if you no longer want \
      to include it in your decision maker. You can also \
      deactivate a statement if it is newly added and you want to await sufficient \
      expert scores to be added before adding it to your decision maker.</p>";
  }

  ngOnInit() {
    // Retrieve all statements for current project and emit them.
    this.statementsCollectionSub = combineLatest([
      this.sessionService.statementsCollection$,
      this.sessionService.project$,
    ]).subscribe(([statementsCollection, project]) => {
      if (statementsCollection && project) {
        statementsCollection
          .aggregate([
            {
              $match: {
                project: project._id,
              },
            },
            {
              $sort: {
                active: -1,
                name: 1,
              },
            },
          ])
          .then((statements) => {
            this.statements$.next(statements);
          });
      }
    });

    // Update the UI to match any changes in statements.
    this.statementsSub = this.statements$.subscribe((statements) => {
      this.statementsFormArray.controls = [];
      if (statements) {
        for (let statement of statements) {
          this.statementsFormArray.push(
            new UntypedFormGroup({
              active: new UntypedFormControl(statement.active, Validators.required),
              label: new UntypedFormControl(statement.label, Validators.required),
              description: new UntypedFormControl(
                statement.description,
                Validators.required
              ),
            })
          );
        }
      }
    });
  }

  ngOnDestroy() {
    this.statementsCollectionSub.unsubscribe();
    this.statementsSub.unsubscribe();
  }

  onSaveNew() {
    // Insert a new statement in the database and then retrieve it to update the app.
    combineLatest([
      this.sessionService.statementsCollection$.pipe(take(1)),
      this.sessionService.project$.pipe(take(1)),
      this.statements$.pipe(take(1)),
    ]).subscribe(([statementsCollection, project, statements]) => {
      statementsCollection
        .insertOne({
          active: true,
          project: project._id,
          ...this.newStatementForm.value,
        })
        .then((id) => {
          statementsCollection
            .findOne({ _id: id.insertedId })
            .then((newItem) => {
              statements.push(newItem);
              this.statements$.next(statements);
            })
            .catch((error) => console.log(error));
        });
    });

    this.newStatementForm.reset();
    this.editingNewStatement = false;
  }

  onCancelNew() {
    this.newStatementForm.reset();
    this.editingNewStatement = false;
  }

  onSaveExisting(index) {
    let statementFormReference = (<UntypedFormArray>(
      this.statementsForm.get("statements")
    )).at(index);
    combineLatest([
      this.sessionService.statementsCollection$.pipe(take(1)),
      this.statements$.pipe(take(1)),
    ]).subscribe(([statementsCollection, statements]) => {
      // Assemble the updated document; project and active need to be retrieved from db:
      //  - project because it's not on the form;
      //  - active because the toggle in the control is disabled during editing and that makes its value unavailable at this point in the code.
      const updatedStatement = {
        project: statements[index].project,
        active: statements[index].active,
        ...statementFormReference.value,
      };

      // Update the document in the db, then retrieve it to keep ui consistent with db
      statementsCollection
        .updateOne({ _id: statements[index]._id }, updatedStatement)
        .then(() => {
          statementsCollection
            .findOne({ _id: statements[index]._id })
            .then((updatedDoc) => {
              statements[index] = updatedDoc;
              this.statements$.next(statements);
            })
            .catch((error) => console.log(error));
        });
    });

    // Reset the form state
    statementFormReference.markAsPristine();
    statementFormReference.markAsUntouched();
    statementFormReference.get("active").enable();
  }

  onCancelModify(index) {
    let statementFormReference = (<UntypedFormArray>(
      this.statementsForm.get("statements")
    )).at(index);
    // Pluck the old statement state from the items$ observable and revert form.
    this.statements$.pipe(take(1)).subscribe((statements) => {
      statementFormReference.setValue({
        active: statements[index].active,
        label: statements[index].label,
        description: statements[index].description,
      });
    });

    // Update form state
    statementFormReference.markAsPristine();
    statementFormReference.get("active").enable();
  }

  onAddStatementForm() {
    this.editingNewStatement = true;
  }

  onToggleActive(index) {
    // Get the toggled statement from db, update the active state and get updated doc from db to keep ui uptodate.
    combineLatest([
      this.sessionService.statementsCollection$.pipe(take(1)),
      this.statements$.pipe(take(1)),
    ]).subscribe(([statementsCollection, statements]) => {
      statementsCollection
        .findOne({ _id: statements[index]._id })
        .then((statement) => {
          // Here's the actual operation
          statement.active = !statement.active;
          statementsCollection
            .updateOne({ _id: statements[index]._id }, statement)
            .then(() => {
              statementsCollection
                .findOne({ _id: statements[index]._id })
                .then((statement) => {
                  statements[index] = statement;
                  this.statements$.next(statements);
                })
                .catch((error) => console.log(error));
            });
        });

      // Reset form state
      (<UntypedFormArray>this.statementsForm.get("statements"))
        .at(index)
        .markAsPristine();
    });
  }

  getStatementControls() {
    const statements: any = this.statementsForm.get("statements");
    return statements.controls;
  }

  showInstructionsDialog(): void {
    this.dialog.open(InfoDialogComponent, {
      width: "400px",
      data: { html: this.instructions, title: "Instructions" },
    });
  }

  onKeydownNewStatement(event) {
    if (
      (event.key === "Enter" || (event.key === "s" && event.ctrlKey)) &&
      this.newStatementForm.valid
    ) {
      this.onSaveNew();
    }
    if (event.key === "Escape" || (event.key === "z" && event.ctrlKey)) {
      this.onCancelNew();
    }
  }

  onKeydownEditStatement(event, index) {
    let statementsFormRef = (<UntypedFormArray>(
      this.statementsForm.get("statements")
    )).at(index);
    if (
      (event.key === "Enter" || (event.key === "s" && event.ctrlKey)) &&
      statementsFormRef.valid
    ) {
      this.onSaveExisting(index);
    } else if (event.key === "Escape" || (event.key === "z" && event.ctrlKey)) {
      this.onCancelModify(index);
    } else {
      statementsFormRef.get("active").disable();
    }
  }
}
