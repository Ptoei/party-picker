// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAK2JDVF5StZ2IJ3NZWnyxCXDg0mLPSGj4",
    authDomain: "picker-273fb.firebaseapp.com",
    databaseURL: "https://picker-273fb.firebaseio.com",
    projectId: "picker-273fb",
    storageBucket: "picker-273fb.appspot.com",
    messagingSenderId: "697933551650",
    appId: "1:697933551650:web:0c638b5276edefef0c527b",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
